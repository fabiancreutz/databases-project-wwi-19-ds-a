from flask import Blueprint, render_template, request, flash, jsonify
from flask_login import login_required, current_user
from .models import Hashtag, Note
from . import db
import json
from .models import Brand
from .models import Clothing
from .models import Hashtag
from .models import Like
from .models import Style
#from .models import chooses
from sqlalchemy import asc, desc

views = Blueprint('views', __name__)


@views.route('/', methods=['GET', 'POST'])
@login_required
def home():
    if request.method == 'POST':
        note = request.form.get('note')

        if len(note) < 1:
            flash('Note is too short!', category='error')
        else:
            new_note = Note(data=note, user_id=current_user.id)
            db.session.add(new_note)
            db.session.commit()
            flash('Note added!', category='success')

    return render_template("home.html", user=current_user)



@views.route('/delete-note', methods=['POST'])
def delete_note():
    note = json.loads(request.data)
    noteId = note['noteId']
    note = Note.query.get(noteId)
    if note:
        if note.user_id == current_user.id:
            db.session.delete(note)
            db.session.commit()

    return jsonify({})


@views.route('/editor', methods=['GET', 'POST'])
@login_required
def editor():
    if request.method == 'POST':

        #Kopfbedeckung wird in der Datenbank gespeichert
        name_Kopfbedekung = request.form.get('NameKopfbedeckung')
        url_Kopfbedekung = request.form.get('urlKopfbedeckung')
        if True:
            new_Kopfbedeckung = Clothing(product_name=name_Kopfbedekung, pic_url=url_Kopfbedekung, user_id=current_user.id)
            db.session.add(new_Kopfbedeckung)
            db.session.commit()
            flash('Kopfbedeckung erstellt!', category='success')
            id_Kopf = new_Kopfbedeckung.product_id
            flash(id_Kopf, category='success')

        
        #Oberteil wird in der Datenbank gespeichert
        name_Oberteil = request.form.get('NameOberteil')
        url_Oberteil = request.form.get('urlOberteil')
        if True:
            new_Oberteil = Clothing(product_name=name_Kopfbedekung, pic_url=url_Kopfbedekung, user_id=current_user.id)
            db.session.add(new_Oberteil)
            db.session.commit()
            flash('Oberteil erstellt!', category='success')

        #Unterteil wird in der Datenbank gespeichert
        name_Unterteil = request.form.get('NameUnterteil')
        url_Unterteil = request.form.get('urlUnterteil')
        if True:
            new_Unterteil = Clothing(product_name=name_Kopfbedekung, pic_url=url_Kopfbedekung, user_id=current_user.id)
            db.session.add(new_Unterteil)
            db.session.commit()
            flash('Unterteil erstellt!', category='success')

        #Schuh wird in der Datenbank gespeichert
        name_Schuhe = request.form.get('NameSchuhe')
        url_Schuhe = request.form.get('urlSchuhe')
        if True:
            new_Schuhe = Clothing(product_name=name_Kopfbedekung, pic_url=url_Kopfbedekung, user_id=current_user.id)
            db.session.add(new_Schuhe)
            db.session.commit()
            flash('Schuhe erstellt!', category='success')


        #Style wird aus den 4 Kleidungsstücken erzeugt und in der Datenbank gespeichert
        kopfbedeckung_id = new_Kopfbedeckung.product_id
        oberteil_id = new_Oberteil.product_id
        unterteil_id = new_Unterteil.product_id
        schuhe_id = new_Schuhe.product_id
        hashtag = request.form.get('Hashtag')

        new_style = Style(user_id=current_user.id, head_id=kopfbedeckung_id, top_id=oberteil_id, lower_part_id=unterteil_id, shoe_id=schuhe_id)
        db.session.add(new_style)
        db.session.commit()
        flash('Style gespeichert', category='success')


        #Hashtag wird gespeichert
        tag = request.form.get('Hashtag')
        style_id = new_style.id

        new_hashtag = Hashtag(tag=tag, style_id=style_id, user_id=current_user.id)
        db.session.add(new_hashtag)
        db.session.commit()
        flash('Hashtag gespeichert', category='success')


    return render_template("editor.html", user=current_user)
    
@views.route('/gespeicherteStyles', methods=['GET', 'POST'])
@login_required
def gespeicherteStyles():
    return render_template("gespeicherteStyles.html", user=current_user)
