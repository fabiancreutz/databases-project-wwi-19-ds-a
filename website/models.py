from . import db
from flask_login import UserMixin
from sqlalchemy.sql import func


class Note(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    data = db.Column(db.String(10000))
    date = db.Column(db.DateTime(timezone=True), default=func.now())
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))




class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(150), unique=True)
    password = db.Column(db.String(150))
    first_name = db.Column(db.String(150))
    last_name = db.Column(db.String(150))
    notes = db.relationship('Note')



class Brand(db.Model):
    brand_id = db.Column(db.Integer, primary_key=True)
    brand_name = db.Column(db.String(150))

class Clothing(db.Model):
    product_id = db.Column(db.Integer, primary_key=True)
    product_name = name = db.Column(db.String(150))
    pic_url = db.Column(db.String(500))
    brand_id = db.Column(db.Integer, db.ForeignKey('clothing.product_id'))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))


class Style(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.String(150), db.ForeignKey('user.id'))
    head_id = db.Column(db.Integer, db.ForeignKey('clothing.product_id'))
    top_id = db.Column(db.Integer, db.ForeignKey('clothing.product_id'))
    lower_part_id = db.Column(db.Integer, db.ForeignKey('clothing.product_id'))
    shoe_id = db.Column(db.Integer, db.ForeignKey('clothing.product_id'))
    #likes = db.relationship('Like')


class Hashtag(db.Model):
    tag_id = db.Column(db.Integer, primary_key=True)
    tag = db.Column(db.String(150))
    style_id = db.Column(db.Integer, db.ForeignKey('style.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))


class Like(db.Model):
    like_id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    style_id = db.Column(db.Integer, db.ForeignKey('style.id'))

#class chooses(db.Model):
#    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
#    product_id = db.Column(db.Integer, db.ForeignKey('clothing.product_id'))





