FROM ubuntu:latest

RUN apt-get update -y 
RUN apt-get install -y python3-pip python-dev

COPY requirements.txt ./requirements.txt

RUN pip3 install -r requirements.txt

COPY . /

ENTRYPOINT [ "python3" ]

CMD [ "main.py" ]


# docker build -t registry.gitlab.com/femu/teamup/api .
# docker push registry.gitlab.com/femu/teamuup/api
